import { useEffect, useState } from "react";

const Count = () => {
    const [count, setCount] = useState(0);

    const clickButtonHandler = () => {
        setCount(count + 1);
    }

    useEffect(() => {
        document.title = `You clicked ${count} times`;
    }, [count]);

    return (
        <div>
            <p>You clicked {count} times</p>
            <button onClick={clickButtonHandler}>Click me</button>
        </div>
    )
}

export default Count;